<?php

use App\Models\Company;
use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\ChBController;
use App\Http\Controllers\DelController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::delete('delete',[CompanyController::class,'destroyMultiple'])->name('deleteMuliple');
Route::resource('companies',CompanyController::class)->middleware('auth');
Route::resource('categories',CategoryController::class);
Route::get('companies.index',function(){

   // return "you must be logging";
return redirect()->route('login.show');

})->name('login');
Route::get('/download/{name}', [CompanyController::class,'downloadfile'])->name('download');
Route::put('delImage/{id}',[DelController::class,'delPicture'])->middleware('auth')->name('delImage');
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['namespace' => 'App\Http\Controllers'], function()
{   
    /**
     * Home Routes
     */
    Route::get('/', 'HomeController@index')->name('home.index');

    Route::group(['middleware' => ['guest']], function() {
        /**
         * Register Routes
         */
        Route::get('/register', 'RegisterController@show')->name('register.show');
        Route::post('/register', 'RegisterController@register')->name('register.perform');

        /**
         * Login Routes
         */
        Route::get('/login', 'LoginController@show')->name('login.show');
        Route::post('/login', 'LoginController@login')->name('login.perform');
       
    });

    Route::group(['middleware' => ['auth']], function() {
        /**
         * Logout Routes
         */

        Route::get('/logout', 'LogoutController@perform')->name('logout.perform');
    });
});