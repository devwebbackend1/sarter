@extends('layouts.sidebar') 
      @section('sidebare')
  

   <div class="row">
    <div><a href="{{ route('home.index') }}">home</a></div>
   </div>
<div class="container">
@if (session('succes'))
    <div class="alert alert-success" role="alert">
        {{ session('succes') }}
    </div>
@endif

    <h1>Company </h1>
    <br>
    <br>
    <a href="{{ route('companies.create')}}" > <button class="btn btn-success">Creat Company</button></a>
    <form  action="{{ route('deleteMuliple')}}" method="post">
   @csrf 
  @method('DELETE')
       <button type="submit"class="btn btn-success">Delete Multipel</button>
   
    <br>
    <br>
    <table border=1  class="table">
      <th scope="col">   </th>
        <th scope="col">  s.no  </th>
        <th scope="col">  Company name </th>
        <th scope="col">  Company Email </th>
        <th scope="col">  Address </th>
        <th scope="col">  Image </th>
        <th scope="col"> Delete image </th>
        <th scope="col">  Edit </th>
        <th scope="col">  Delete </th>
     
        @foreach($companies as $company)
      
        <tr><td><div class="form-check">
          <input class="form-check-input" name="companies[]" type="checkbox" value="{{$company->id}}" id="flexCheckChecked" >
          <label class="form-check-label" for="flexCheckChecked">
            Checked checkbox
          </label>
      
        </div></td>
            <td>{{$company->id}}</td>
            <td>{{$company->name}}</td>
            <td>{{$company->email}}</td>
            <td>{{$company->address}}</td>
            @if($company->path == 1)
          
          <td>
	     <img src={{ url('public/Image/'.$company->picture) }}
 style="height: 100px; width: 150px;">
 <a href="{{route('download',$company->picture)}}" class="btn btn-large pull-right" ><i class="icon-download-alt"> </i> Download  </a></td>
  
@endif


        
           
          <td> <a href={{route('companies.edit', $company->id )}}> <button class="btn btn-primary">edit</button></a></td>
           
          <form action="{{route('companies.destroy', $company->id)}}" method="post"> 
          @csrf 
          @method('DELETE')
            <td>  <button type="submit" class="btn btn-danger">delete</button></td>
          </form>
        </tr>
      
        @endforeach

       
      </form>   


</table>



{!! $companies->links() !!}
</div>
@endsection
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

</body>

</html>