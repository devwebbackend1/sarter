<?php

namespace App\Http\Controllers;
use App\Models\Company;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Collection;

class CompanyController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }*/
    public function index()
    {
        $data['companies'] = Company::orderBy('id','desc')->paginate(5);
        return view('companies.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'address'=>'required',
            'image' => 'required'
            //'image' => 'required|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        //var_dump($request->hasfile('image'));die;

        $company = new Company();
        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('public/Image'), $filename);
            $company->picture = $filename;
        }
        
        $company->name = $request->name;
        $company->email = $request->email;
        $company->address = $request->address;
        $company->path = 1;
       
       // $company->picture= $name;
        $company->save();
      //return dd($name);
         return redirect()->route('companies.index')->with('succes',"your data is saved succefully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        
       return view('companies.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Company $company)
    { $request->validate([
        'name'=>'required',
        'email'=>'required',
        'address'=>'required',
        'image' => 'required|mimes:jpg,png,jpeg,gif,svg|max:2048',
    ]);
    //$company=Compay::find($id);

    $file= $request->file('image');
    $filename= date('YmdHi').$file->getClientOriginalName();
     $file-> move(public_path('public/Image'), $filename);
     $content= file_get_contents($filename);
     Response($content)->withHeaders([
        'Content-Type'=>mime_content_type($filename),
     ]);
   $company->picture=$filename;
   $company->path= 1;
    $company->name = $request->name;
    $company->email = $request->email;
    $company->address = $request->address;
    $company->save(); 
    return redirect()->route('companies.index')->with('succes','your data updated succesfully');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    { 
        $company->delete();

        return redirect()->route('companies.index')->with('succes','your data deleted succesfully');
    }
    public function downloadfile($name)
    {

$filename = public_path("\\public\\image\\")."".$name;
       
     return response()->download($filename);
    }
    public function destroyMultiple(Request $request)
    { $company=$request->get('companies');
      // var_dump($company);die;
       /* foreach($company as $comp){
       Company::find($comp)->delete();
     
    }*/
    Company::whereIn('id',$company)->delete();
        return redirect()->route('companies.index')->with('succes','your data deleted succesfully');
    }
}


  
